### Novas funcionalidades 

* [MF003](https://trello.com/c/DQ8jotkr/4-003-back-criar-estrutura-para-crud) - Criado estrutura para CRUD
* [MF005](https://trello.com/c/jj4e3tbX/5-mf005-cadastro-de-usu%C3%A1rio) - Criado cadastro de usuário
* [MF006](https://trello.com/c/5dbgx0YX/7-mf006-cadastro-de-categorias) - Criado cadastro de categoria

### Melhorias
* N/A.

### Correções
* N/A.
