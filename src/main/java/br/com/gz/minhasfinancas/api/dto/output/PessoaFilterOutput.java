package br.com.gz.minhasfinancas.api.dto.output;

import br.com.gz.minhasfinancas.api.model.Pessoa;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PessoaFilterOutput extends OutputDefault {

    private List<Pessoa> contents;

    public PessoaFilterOutput(List<Pessoa> contents, Long totalElements) {
        super(totalElements);
        this.contents = contents;
    }
}
