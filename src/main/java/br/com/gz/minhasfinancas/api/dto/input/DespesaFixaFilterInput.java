package br.com.gz.minhasfinancas.api.dto.input;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DespesaFixaFilterInput extends InputDefault {

    private String descricao;
    private BigDecimal vlrDespesa;
    private Integer diaVencimento;
}
