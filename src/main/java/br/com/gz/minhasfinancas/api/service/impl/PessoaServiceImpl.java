package br.com.gz.minhasfinancas.api.service.impl;

import br.com.gz.minhasfinancas.api.dto.input.PessoaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.PessoaFilterOutput;
import br.com.gz.minhasfinancas.api.model.Pessoa;
import br.com.gz.minhasfinancas.api.predicate.PessoaPredicate;
import br.com.gz.minhasfinancas.api.repository.PessoaRepository;
import br.com.gz.minhasfinancas.api.service.PessoaService;
import br.com.gz.minhasfinancas.api.util.Utils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class PessoaServiceImpl extends CrudServiceImpl<Pessoa, PessoaFilterInput, PessoaFilterOutput, UUID>
        implements PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Override
    public PessoaFilterOutput findAllByFilter(PessoaFilterInput inputFilter) {
        PageRequest page = Utils.getPageRequest(inputFilter.getPageRequest());
        OrderSpecifier<?> orderBy = Utils.getOrderBy(inputFilter.getPageRequest());

        List<Pessoa> listOutput = pessoaRepository.findAllByPredicate(getPredicate(inputFilter), page, orderBy);
        Long totalElements = pessoaRepository.getTotalElements(getPredicate(inputFilter));
        return new PessoaFilterOutput(listOutput, totalElements);
    }

    @Override
    protected JpaRepository<Pessoa, UUID> getRepository() {
        return pessoaRepository;
    }

    private Predicate getPredicate(PessoaFilterInput pessoaFilterInput) {
        return new PessoaPredicate()
                .buildPredicate()
                .filterCodigo(pessoaFilterInput.getCodigo())
                .filterNome(pessoaFilterInput.getNome())
                .getPredicate();
    }

    @Override
    public List<Pessoa> complete(String query) {
        if (StringUtils.hasText(query)) {
            return pessoaRepository.findAllByNomeLikeIgnoreCase("%".concat(query).concat("%"));
        }
        return pessoaRepository.findAll();
    }
}
