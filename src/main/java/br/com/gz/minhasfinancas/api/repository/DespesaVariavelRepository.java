package br.com.gz.minhasfinancas.api.repository;

import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import br.com.gz.minhasfinancas.api.repository.custom.DespesaVariavelRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DespesaVariavelRepository extends JpaRepository<DespesaVariavel, UUID>, DespesaVariavelRepositoryCustom {
}
