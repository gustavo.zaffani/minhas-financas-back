package br.com.gz.minhasfinancas.api.predicate;

import br.com.gz.minhasfinancas.api.model.QCategoria;
import br.com.gz.minhasfinancas.api.util.PredicateBase;

import java.util.Objects;

public class CategoriaPredicate extends PredicateBase<CategoriaPredicate> {

    public CategoriaPredicate filterCodigo(Long codigo) {
        if (Objects.nonNull(codigo)) {
            getBooleanBuilder().and(QCategoria.categoria.codigo.eq(codigo));
        }
        return this;
    }

    public CategoriaPredicate filterDescricao(String descricao) {
        if (Objects.nonNull(descricao)) {
            getBooleanBuilder().and(QCategoria.categoria.descricao.toLowerCase().like("%" +  descricao.toLowerCase() + "%"));
        }
        return this;
    }
}
