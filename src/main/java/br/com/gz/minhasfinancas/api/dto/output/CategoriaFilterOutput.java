package br.com.gz.minhasfinancas.api.dto.output;

import br.com.gz.minhasfinancas.api.model.Categoria;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CategoriaFilterOutput extends OutputDefault {

    private List<Categoria> contents;

    public CategoriaFilterOutput(List<Categoria> categorias, Long totalElements) {
        super(totalElements);
        this.contents = categorias;
    }
}
