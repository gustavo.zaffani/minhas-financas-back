package br.com.gz.minhasfinancas.api.controller;

import br.com.gz.minhasfinancas.api.dto.input.DespesaFixaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.DespesaFixaFilterOutput;
import br.com.gz.minhasfinancas.api.model.DespesaFixa;
import br.com.gz.minhasfinancas.api.service.CrudService;
import br.com.gz.minhasfinancas.api.service.DespesaFixaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("despesa-fixa")
public class DespesaFixaController extends CrudController<DespesaFixa, DespesaFixaFilterInput, DespesaFixaFilterOutput, UUID> {

    @Autowired
    private DespesaFixaService despesaFixaService;

    @Override
    protected CrudService<DespesaFixa, DespesaFixaFilterInput, DespesaFixaFilterOutput, UUID> getService() {
        return despesaFixaService;
    }

    @Override
    void preSave(DespesaFixa object) {
        if (Objects.isNull(object.getId())) {
            object.setDtCadastro(LocalDateTime.now());
        }
    }
}
