package br.com.gz.minhasfinancas.api.service.impl;

import br.com.gz.minhasfinancas.api.enumeration.EnumStatusParcela;
import br.com.gz.minhasfinancas.api.model.Cartao;
import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import br.com.gz.minhasfinancas.api.model.Parcela;
import br.com.gz.minhasfinancas.api.service.ParcelaService;
import br.com.gz.minhasfinancas.api.util.BigDecimalUtil;
import br.com.gz.minhasfinancas.api.util.Utils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ParcelaServiceImpl implements ParcelaService {

    @Override
    public List<Parcela> gerarParcelasByDespesaVariavel(DespesaVariavel despesaVariavel) {
        switch (despesaVariavel.getFormaPagamento()) {
            case CARTAO:
                return gerarParcelasByCartao(despesaVariavel);
            case BOLETO:
                return gerarParcelasByBoleto(despesaVariavel);
            default:
                break;
        }
        return null;
    }

    private List<Parcela> gerarParcelasByCartao(DespesaVariavel despesaVariavel) {
        List<Parcela> parcelaList = new ArrayList<>();
        List<BigDecimal> valoresParcelas = getValoresParcelas(despesaVariavel.getVlrDespesa(), despesaVariavel.getQtdParcela());
        Cartao cartaoUtilizado = despesaVariavel.getCartao();

        for (int i = 1; i <= despesaVariavel.getQtdParcela(); i++) {
            Parcela parcela = new Parcela();
            parcela.setSequencia(i);
            parcela.setStatusParcela(EnumStatusParcela.PENDENTE);
            parcela.setDtVencimento(Utils.getNextVencimentoCartaoByParcela(
                    cartaoUtilizado.getMelhorDataCompra(),
                    cartaoUtilizado.getDtVencimento(),
                    despesaVariavel.getDtDespesa(),
                    i)
            );
            parcela.setDespesaVariavel(despesaVariavel);
            parcela.setVlrParcela(valoresParcelas.get(i - 1));
            parcela.setQtdParcela(despesaVariavel.getQtdParcela());
            parcelaList.add(parcela);
        }
        return parcelaList;
    }

    private List<Parcela> gerarParcelasByBoleto(DespesaVariavel despesaVariavel) {
        List<Parcela> parcelaList = new ArrayList<>();
        List<BigDecimal> valoresParcelas = getValoresParcelas(despesaVariavel.getVlrDespesa(), despesaVariavel.getQtdParcela());

        for (int i = 1; i <= despesaVariavel.getQtdParcela(); i++) {
            Parcela parcela = new Parcela();
            parcela.setSequencia(i);
            parcela.setStatusParcela(EnumStatusParcela.PENDENTE);
            parcela.setDtVencimento(Utils.getNextVencimentoBoletoByParcela(
                    despesaVariavel.getDiaVencimento(), i, despesaVariavel.getDtDespesa()));
            parcela.setDespesaVariavel(despesaVariavel);
            parcela.setVlrParcela(valoresParcelas.get(i - 1));
            parcela.setQtdParcela(despesaVariavel.getQtdParcela());
            parcelaList.add(parcela);
        }

        return parcelaList;
    }

    private List<BigDecimal> getValoresParcelas(BigDecimal vlrDespesa, Integer qtdParcela) {
        List<BigDecimal> valoresParcelas = new ArrayList<>();

        if (qtdParcela == 1) {
            return Arrays.asList(vlrDespesa);
        }

        BigDecimal vlrParcela = vlrDespesa
                .divide(BigDecimal.valueOf(qtdParcela), MathContext.DECIMAL32)
                .setScale(2, RoundingMode.HALF_UP);

        for (int i = 1; i < qtdParcela; i++) {
            valoresParcelas.add(vlrParcela);
        }
        BigDecimal vlrUltimaParcela = vlrDespesa.subtract(BigDecimalUtil.sumValues(valoresParcelas));
        valoresParcelas.add(vlrUltimaParcela);

        return valoresParcelas;
    }
}
