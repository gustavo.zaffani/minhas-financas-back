package br.com.gz.minhasfinancas.api.dto.output;

import br.com.gz.minhasfinancas.api.model.Usuario;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UsuarioFilterOutput extends OutputDefault {

    private List<Usuario> contents;

    public UsuarioFilterOutput(List<Usuario> usuarios, Long totalElements) {
        super(totalElements);
        this.contents = usuarios;
    }
}
