package br.com.gz.minhasfinancas.api.predicate;

import br.com.gz.minhasfinancas.api.model.QDespesaFixa;
import br.com.gz.minhasfinancas.api.util.PredicateBase;

import java.math.BigDecimal;
import java.util.Objects;

public class DespesaFixaPredicate extends PredicateBase<DespesaFixaPredicate> {

    public DespesaFixaPredicate filterDescricao(String descricao) {
        if (Objects.nonNull(descricao)) {
            getBooleanBuilder().and(QDespesaFixa.despesaFixa.descricao.toLowerCase().like("%" +  descricao.toLowerCase() + "%"));
        }
        return this;
    }

    public DespesaFixaPredicate filterVlrDespesa(BigDecimal vlrDespesa) {
        if (Objects.nonNull(vlrDespesa)) {
            getBooleanBuilder().and(QDespesaFixa.despesaFixa.vlrDespesa.eq(vlrDespesa));
        }
        return this;
    }

    public DespesaFixaPredicate filterDiaVencimento(Integer diaVencimento) {
        if (Objects.nonNull(diaVencimento)) {
            getBooleanBuilder().and(QDespesaFixa.despesaFixa.diaVencimento.eq(diaVencimento));
        }
        return this;
    }
}
