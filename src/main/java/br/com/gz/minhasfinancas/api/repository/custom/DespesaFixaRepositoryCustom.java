package br.com.gz.minhasfinancas.api.repository.custom;

import br.com.gz.minhasfinancas.api.model.DespesaFixa;

public interface DespesaFixaRepositoryCustom extends CustomRepository<DespesaFixa> {
}
