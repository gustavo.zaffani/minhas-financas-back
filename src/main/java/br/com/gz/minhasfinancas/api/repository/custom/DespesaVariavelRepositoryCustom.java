package br.com.gz.minhasfinancas.api.repository.custom;

import br.com.gz.minhasfinancas.api.model.DespesaVariavel;

public interface DespesaVariavelRepositoryCustom extends CustomRepository<DespesaVariavel> {
}
