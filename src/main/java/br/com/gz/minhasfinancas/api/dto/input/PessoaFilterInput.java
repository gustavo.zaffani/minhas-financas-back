package br.com.gz.minhasfinancas.api.dto.input;

import lombok.Data;

@Data
public class PessoaFilterInput extends InputDefault {

    private Long codigo;
    private String nome;
}
