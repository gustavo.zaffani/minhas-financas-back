package br.com.gz.minhasfinancas.api.dto.output;

import br.com.gz.minhasfinancas.api.model.Cartao;
import lombok.Data;

import java.util.List;

@Data
public class CartaoFilterOutput extends OutputDefault {

    private List<Cartao> contents;

    public CartaoFilterOutput(Long totalElements, List<Cartao> contents) {
        super(totalElements);
        this.contents = contents;
    }
}
