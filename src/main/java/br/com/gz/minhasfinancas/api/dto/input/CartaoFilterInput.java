package br.com.gz.minhasfinancas.api.dto.input;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CartaoFilterInput extends InputDefault {

    private Long codigo;
    private String descricao;
    private Integer dtVencimento;
    private Integer melhorDataCompra;
    private BigDecimal limite;
}
