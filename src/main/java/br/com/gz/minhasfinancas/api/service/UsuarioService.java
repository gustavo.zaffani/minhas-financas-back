package br.com.gz.minhasfinancas.api.service;

import br.com.gz.minhasfinancas.api.dto.input.UsuarioFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.UsuarioFilterOutput;
import br.com.gz.minhasfinancas.api.model.Usuario;

import java.util.UUID;

public interface UsuarioService extends CrudService<Usuario, UsuarioFilterInput, UsuarioFilterOutput, UUID> {
}
