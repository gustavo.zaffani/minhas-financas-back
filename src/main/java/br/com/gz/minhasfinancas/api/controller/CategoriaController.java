package br.com.gz.minhasfinancas.api.controller;

import br.com.gz.minhasfinancas.api.dto.input.CategoriaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.CategoriaFilterOutput;
import br.com.gz.minhasfinancas.api.model.Categoria;
import br.com.gz.minhasfinancas.api.service.CategoriaService;
import br.com.gz.minhasfinancas.api.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("categoria")
public class CategoriaController extends CrudController<Categoria, CategoriaFilterInput, CategoriaFilterOutput, UUID> {

    @Autowired
    private CategoriaService categoriaService;

    @Override
    protected CrudService<Categoria, CategoriaFilterInput, CategoriaFilterOutput, UUID> getService() {
        return categoriaService;
    }
}
