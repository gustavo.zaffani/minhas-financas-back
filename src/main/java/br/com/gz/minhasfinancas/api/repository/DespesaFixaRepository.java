package br.com.gz.minhasfinancas.api.repository;

import br.com.gz.minhasfinancas.api.model.DespesaFixa;
import br.com.gz.minhasfinancas.api.repository.custom.DespesaFixaRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DespesaFixaRepository extends JpaRepository<DespesaFixa, UUID>, DespesaFixaRepositoryCustom {
}
