package br.com.gz.minhasfinancas.api.dto.input;

import br.com.gz.minhasfinancas.api.enumeration.EnumFormaPagamento;
import br.com.gz.minhasfinancas.api.enumeration.EnumStatusDespesa;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class DespesaVariavelFilterInput extends InputDefault {

    private String descricao;
    private BigDecimal vlrDespesa;
    private EnumStatusDespesa statusDespesa;
    private EnumFormaPagamento formaPagamento;
}
