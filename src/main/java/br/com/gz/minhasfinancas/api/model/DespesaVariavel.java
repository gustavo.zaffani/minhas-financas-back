package br.com.gz.minhasfinancas.api.model;

import br.com.gz.minhasfinancas.api.config.LocalDateDeserializer;
import br.com.gz.minhasfinancas.api.config.LocalDateSerializer;
import br.com.gz.minhasfinancas.api.config.LocalDateTimeDeserializer;
import br.com.gz.minhasfinancas.api.config.LocalDateTimeSerializer;
import br.com.gz.minhasfinancas.api.enumeration.EnumFormaPagamento;
import br.com.gz.minhasfinancas.api.enumeration.EnumStatusDespesa;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "despesaVariavel")
public class DespesaVariavel {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotEmpty(message = "O campo 'descricao' não pode ser vazio.")
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @NotNull
    @Column(name = "dtcadastro", nullable = false)
    private LocalDateTime dtCadastro;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @NotNull(message = "O campo 'data da despesa' não pode ser nulo.")
    @Column(name = "dtdespesa", nullable = false)
    private LocalDate dtDespesa;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "O campo 'forma de pagamento' não pode ser nulo.")
    @Column(name = "formapagamento", nullable = false)
    private EnumFormaPagamento formaPagamento;

    @ManyToOne
    @JoinColumn(name = "categoria_id", referencedColumnName = "id")
    private Categoria categoria;

    @ManyToOne
    @JoinColumn(name = "cartao_id", referencedColumnName = "id")
    private Cartao cartao;

    @Column(name = "qtdparcela")
    private Integer qtdParcela;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private EnumStatusDespesa statusDespesa;

    @NotNull(message = "O campo 'valor da despesa' não pode ser nulo.")
    @Column(name = "vlrdespesa", nullable = false)
    private BigDecimal vlrDespesa;

    @Column(name = "diavencimento")
    private Integer diaVencimento;

    @Column(name = "observacao", length = 500)
    private String observacao;

    @OneToMany(mappedBy = "despesaVariavel",
            cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JsonManagedReference
    private List<Parcela> parcelas;

    @NotNull
    @OneToMany(mappedBy = "despesaVariavel",
            cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JsonManagedReference
    private List<DespesaRateio> rateios;
}
