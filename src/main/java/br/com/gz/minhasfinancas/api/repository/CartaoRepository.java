package br.com.gz.minhasfinancas.api.repository;

import br.com.gz.minhasfinancas.api.model.Cartao;
import br.com.gz.minhasfinancas.api.repository.custom.CartaoRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CartaoRepository extends JpaRepository<Cartao, UUID>, CartaoRepositoryCustom {

    List<Cartao> findAllByDescricaoLikeIgnoreCase(String query);
}
