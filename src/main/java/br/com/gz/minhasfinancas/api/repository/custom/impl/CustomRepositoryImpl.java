package br.com.gz.minhasfinancas.api.repository.custom.impl;

import br.com.gz.minhasfinancas.api.repository.custom.CustomRepository;
import com.querydsl.core.types.Predicate;
import org.springframework.transaction.annotation.Transactional;

public abstract class CustomRepositoryImpl<T> implements CustomRepository<T> {

    @Override
    @Transactional(readOnly = true)
    public Long getTotalElements(Predicate predicate) {
        return getBaseQuery(predicate).fetchCount();
    }
}
