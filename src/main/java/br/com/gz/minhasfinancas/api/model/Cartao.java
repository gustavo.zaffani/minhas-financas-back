package br.com.gz.minhasfinancas.api.model;

import br.com.gz.minhasfinancas.api.enumeration.EnumTipoCartao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "cartao")
public class Cartao {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull(message = "O campo 'codigo' não pode ser nulo.")
    @Column(name = "codigo", nullable = false, unique = true)
    private Long codigo;

    @NotEmpty(message = "O campo 'descricao' não pode ser vazio.")
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Min(value = 1, message = "O menor dia possível para ser informado é '1'")
    @Max(value = 31, message = "O menor dia possível para ser informado é '31'")
    @Column(name = "dtVencimento")
    private Integer dtVencimento;

    @Min(value = 1, message = "O menor dia possível para ser informado é '1'")
    @Max(value = 31, message = "O menor dia possível para ser informado é '31'")
    @Column(name = "melhorDataCompra")
    private Integer melhorDataCompra;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipoCartao")
    private EnumTipoCartao tipoCartao;

    @Column(name = "limite")
    private BigDecimal limite;

    @Column(name = "limiteDisponivel")
    private BigDecimal limiteDisponivel;
}
