package br.com.gz.minhasfinancas.api.dto.input;

import lombok.Data;

@Data
public class UsuarioFilterInput extends InputDefault {

    private String nome;
    private String usuario;
    private String email;
}
