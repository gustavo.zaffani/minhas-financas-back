package br.com.gz.minhasfinancas.api.service.impl;

import br.com.gz.minhasfinancas.api.dto.input.CartaoFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.CartaoFilterOutput;
import br.com.gz.minhasfinancas.api.model.Cartao;
import br.com.gz.minhasfinancas.api.predicate.CartaoPredicate;
import br.com.gz.minhasfinancas.api.repository.CartaoRepository;
import br.com.gz.minhasfinancas.api.service.CartaoService;
import br.com.gz.minhasfinancas.api.util.Utils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class CartaoServiceImpl extends CrudServiceImpl<Cartao, CartaoFilterInput, CartaoFilterOutput, UUID>
    implements CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Override
    protected JpaRepository<Cartao, UUID> getRepository() {
        return cartaoRepository;
    }

    @Override
    public CartaoFilterOutput findAllByFilter(CartaoFilterInput inputFilter) {
        PageRequest page = Utils.getPageRequest(inputFilter.getPageRequest());
        OrderSpecifier<?> orderBy = Utils.getOrderBy(inputFilter.getPageRequest());

        List<Cartao> listOutput = cartaoRepository.findAllByPredicate(getPredicate(inputFilter), page, orderBy);
        Long totalElements = cartaoRepository.getTotalElements(getPredicate(inputFilter));

        return new CartaoFilterOutput(totalElements, listOutput);
    }

    private Predicate getPredicate(CartaoFilterInput cartaoFilterInput) {
        return new CartaoPredicate()
                .buildPredicate()
                .filterCodigo(cartaoFilterInput.getCodigo())
                .filterDescricao(cartaoFilterInput.getDescricao())
                .filterDtVencimento(cartaoFilterInput.getDtVencimento())
                .filterMelhorDataCompra(cartaoFilterInput.getMelhorDataCompra())
                .filterLimite(cartaoFilterInput.getLimite())
                .getPredicate();
    }

    @Override
    public List<Cartao> complete(String query) {
        if (StringUtils.hasText(query)) {
            return cartaoRepository.findAllByDescricaoLikeIgnoreCase("%".concat(query).concat("%"));
        }
        return cartaoRepository.findAll();
    }
}
