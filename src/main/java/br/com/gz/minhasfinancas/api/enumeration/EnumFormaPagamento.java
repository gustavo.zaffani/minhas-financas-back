package br.com.gz.minhasfinancas.api.enumeration;

public enum EnumFormaPagamento {
    DINHEIRO,
    CARTAO,
    BOLETO
}
