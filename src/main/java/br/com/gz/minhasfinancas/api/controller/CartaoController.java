package br.com.gz.minhasfinancas.api.controller;

import br.com.gz.minhasfinancas.api.dto.input.CartaoFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.CartaoFilterOutput;
import br.com.gz.minhasfinancas.api.model.Cartao;
import br.com.gz.minhasfinancas.api.service.CartaoService;
import br.com.gz.minhasfinancas.api.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("cartao")
public class CartaoController extends CrudController<Cartao, CartaoFilterInput, CartaoFilterOutput, UUID> {

    @Autowired
    private CartaoService cartaoService;

    @Override
    protected CrudService<Cartao, CartaoFilterInput, CartaoFilterOutput, UUID> getService() {
        return cartaoService;
    }
}
