package br.com.gz.minhasfinancas.api.repository;

import br.com.gz.minhasfinancas.api.model.Pessoa;
import br.com.gz.minhasfinancas.api.repository.custom.PessoaRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, UUID>, PessoaRepositoryCustom {

    List<Pessoa> findAllByNomeLikeIgnoreCase(String nome);
}
