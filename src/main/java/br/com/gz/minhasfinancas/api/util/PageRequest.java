package br.com.gz.minhasfinancas.api.util;

import br.com.gz.minhasfinancas.api.enumeration.EnumTypeOrder;
import lombok.Data;

@Data
public class PageRequest {

    private Integer size;
    private Integer offset;
    private String fieldOrder;
    private EnumTypeOrder typeOrder;
}
