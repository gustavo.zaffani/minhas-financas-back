package br.com.gz.minhasfinancas.api.service.impl;

import br.com.gz.minhasfinancas.api.dto.input.DespesaFixaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.DespesaFixaFilterOutput;
import br.com.gz.minhasfinancas.api.model.DespesaFixa;
import br.com.gz.minhasfinancas.api.predicate.DespesaFixaPredicate;
import br.com.gz.minhasfinancas.api.repository.DespesaFixaRepository;
import br.com.gz.minhasfinancas.api.service.DespesaFixaService;
import br.com.gz.minhasfinancas.api.util.Utils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DespesaFixaServiceImpl extends CrudServiceImpl<DespesaFixa, DespesaFixaFilterInput, DespesaFixaFilterOutput, UUID>
        implements DespesaFixaService {

    @Autowired
    private DespesaFixaRepository despesaFixaRepository;

    @Override
    protected JpaRepository<DespesaFixa, UUID> getRepository() {
        return despesaFixaRepository;
    }

    @Override
    public DespesaFixaFilterOutput findAllByFilter(DespesaFixaFilterInput inputFilter) {
        PageRequest page = Utils.getPageRequest(inputFilter.getPageRequest());
        OrderSpecifier<?> orderBy = Utils.getOrderBy(inputFilter.getPageRequest());

        List<DespesaFixa> listOutput = despesaFixaRepository.findAllByPredicate(getPredicate(inputFilter), page, orderBy);
        Long totalElements = despesaFixaRepository.getTotalElements(getPredicate(inputFilter));
        return new DespesaFixaFilterOutput(listOutput, totalElements);
    }

    private Predicate getPredicate(DespesaFixaFilterInput despesaFixaFilterInput) {
        return new DespesaFixaPredicate()
                .buildPredicate()
                .filterDescricao(despesaFixaFilterInput.getDescricao())
                .filterVlrDespesa(despesaFixaFilterInput.getVlrDespesa())
                .filterDiaVencimento(despesaFixaFilterInput.getDiaVencimento())
                .getPredicate();
    }
}
