package br.com.gz.minhasfinancas.api.repository.custom;

import br.com.gz.minhasfinancas.api.model.Cartao;

public interface CartaoRepositoryCustom extends CustomRepository<Cartao> {
}
