package br.com.gz.minhasfinancas.api.dto.output;

import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DespesaVariavelFilterOutput extends OutputDefault {

    private List<DespesaVariavel> contents;

    public DespesaVariavelFilterOutput(Long totalElements, List<DespesaVariavel> contents) {
        super(totalElements);
        this.contents = contents;
    }
}
