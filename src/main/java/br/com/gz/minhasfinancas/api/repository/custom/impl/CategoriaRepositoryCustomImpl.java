package br.com.gz.minhasfinancas.api.repository.custom.impl;

import br.com.gz.minhasfinancas.api.model.Categoria;
import br.com.gz.minhasfinancas.api.model.QCategoria;
import br.com.gz.minhasfinancas.api.repository.custom.CategoriaRepositoryCustom;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

public class CategoriaRepositoryCustomImpl extends CustomRepositoryImpl<Categoria>
        implements CategoriaRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public List<Categoria> findAllByPredicate(Predicate predicate, PageRequest pageRequest, OrderSpecifier<?> order) {
        return getBaseQuery(predicate)
                .orderBy(getOrderBy(order))
                .offset(pageRequest.getOffset())
                .limit(pageRequest.getPageSize())
                .fetch();
    }

    @Override
    public JPAQuery<Categoria> getBaseQuery(Predicate predicate) {
        return new JPAQuery<>(em)
                .select(QCategoria.categoria)
                .from(QCategoria.categoria)
                .where(predicate);
    }

    @Override
    public OrderSpecifier<?> getOrderBy(OrderSpecifier<?> orderBy) {
        if (Objects.isNull(orderBy)) {
            return new OrderSpecifier<>(Order.DESC, QCategoria.categoria.codigo);
        }
        return orderBy;
    }
}
