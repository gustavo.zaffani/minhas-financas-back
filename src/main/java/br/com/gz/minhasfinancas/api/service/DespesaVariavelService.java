package br.com.gz.minhasfinancas.api.service;

import br.com.gz.minhasfinancas.api.dto.input.DespesaVariavelFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.DespesaVariavelFilterOutput;
import br.com.gz.minhasfinancas.api.model.DespesaVariavel;

import java.util.UUID;

public interface DespesaVariavelService extends CrudService<DespesaVariavel, DespesaVariavelFilterInput, DespesaVariavelFilterOutput, UUID> {

    DespesaVariavel saveDespesaVariavel(DespesaVariavel despesaVariavel);
}
