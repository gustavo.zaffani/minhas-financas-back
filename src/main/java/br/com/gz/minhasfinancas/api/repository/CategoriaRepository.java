package br.com.gz.minhasfinancas.api.repository;

import br.com.gz.minhasfinancas.api.model.Categoria;
import br.com.gz.minhasfinancas.api.repository.custom.CategoriaRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, UUID>, CategoriaRepositoryCustom {

    List<Categoria> findAllByDescricaoLikeIgnoreCase(String query);
}
