package br.com.gz.minhasfinancas.api.service.impl;

import br.com.gz.minhasfinancas.api.dto.input.CategoriaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.CategoriaFilterOutput;
import br.com.gz.minhasfinancas.api.model.Categoria;
import br.com.gz.minhasfinancas.api.predicate.CategoriaPredicate;
import br.com.gz.minhasfinancas.api.repository.CategoriaRepository;
import br.com.gz.minhasfinancas.api.service.CategoriaService;
import br.com.gz.minhasfinancas.api.util.Utils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class CategoriaServiceImpl extends CrudServiceImpl<Categoria, CategoriaFilterInput, CategoriaFilterOutput, UUID>
        implements CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Override
    protected JpaRepository<Categoria, UUID> getRepository() {
        return categoriaRepository;
    }

    @Override
    public CategoriaFilterOutput findAllByFilter(CategoriaFilterInput inputFilter) {
        PageRequest page = Utils.getPageRequest(inputFilter.getPageRequest());
        OrderSpecifier<?> orderBy = Utils.getOrderBy(inputFilter.getPageRequest());

        List<Categoria> listOutput = categoriaRepository.findAllByPredicate(getPredicate(inputFilter), page, orderBy);
        Long totalElements = categoriaRepository.getTotalElements(getPredicate(inputFilter));

        return new CategoriaFilterOutput(listOutput, totalElements);
    }

    private Predicate getPredicate(CategoriaFilterInput categoriaFilterInput) {
        return new CategoriaPredicate()
                .buildPredicate()
                .filterCodigo(categoriaFilterInput.getCodigo())
                .filterDescricao(categoriaFilterInput.getDescricao())
                .getPredicate();
    }

    @Override
    public List<Categoria> complete(String query) {
        if (StringUtils.hasText(query)) {
            return categoriaRepository.findAllByDescricaoLikeIgnoreCase("%".concat(query).concat("%"));
        }
        return categoriaRepository.findAll();
    }
}
