package br.com.gz.minhasfinancas.api.util;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import java.util.UUID;

public abstract class PredicateBase<P extends PredicateBase<?>> {

    public static final String JAVAX_PERSISTENCE_LOCK_TIMEOUT = "javax.persistence.lock.timeout";

    public static final String LOCK_TIMEOUT_VALUE = "20000"; // 20 seconds

    public PredicateBase() {
    }

    private BooleanBuilder booleanBuilder;

    @SuppressWarnings("unchecked")
    public P buildPredicate() {
        this.booleanBuilder = null;
        return (P) this;
    }

    public Predicate getPredicate() {
        return booleanBuilder == null ? new BooleanBuilder() : booleanBuilder;
    }

    protected BooleanBuilder getBooleanBuilder() {
        if (booleanBuilder == null) {
            this.booleanBuilder = new BooleanBuilder();
        }
        return this.booleanBuilder;
    }

    public BooleanBuilder or(Predicate... predicates) {
        if (predicates == null)
            return new BooleanBuilder();

        BooleanBuilder output = new BooleanBuilder();
        for (Predicate predicado : predicates) {
            output.or(predicado);
        }
        return output;
    }

    public BooleanBuilder and(Predicate... predicates) {
        if (predicates == null)
            return new BooleanBuilder();

        BooleanBuilder output = new BooleanBuilder();
        for (Predicate predicado : predicates) {
            output.and(predicado);
        }
        return output;
    }

    public String getLikeFormatted(String value) {
        return "%".concat(value.toLowerCase()).concat("%");
    }

    protected UUID getUUID(String id) {
        return java.util.UUID.fromString(id);
    }
}

