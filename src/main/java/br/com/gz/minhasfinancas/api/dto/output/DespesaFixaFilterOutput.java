package br.com.gz.minhasfinancas.api.dto.output;

import br.com.gz.minhasfinancas.api.model.DespesaFixa;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DespesaFixaFilterOutput extends OutputDefault {

    private List<DespesaFixa> contents;

    public DespesaFixaFilterOutput(List<DespesaFixa> contents, Long totalElements) {
        super(totalElements);
        this.contents = contents;
    }
}
