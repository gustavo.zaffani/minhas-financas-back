package br.com.gz.minhasfinancas.api.repository.custom;

import br.com.gz.minhasfinancas.api.model.Usuario;

public interface UsuarioRepositoryCustom extends CustomRepository<Usuario> {
}
