package br.com.gz.minhasfinancas.api.controller;

import br.com.gz.minhasfinancas.api.dto.input.DeleteInput;
import br.com.gz.minhasfinancas.api.service.CrudService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

public abstract class CrudController<T, I, O, ID extends Serializable> {

    protected abstract CrudService<T, I, O, ID> getService();

    @GetMapping
    public List<T> findAll() {
        return getService().findAll(Sort.by("id"));
    }

    @PostMapping("findAllByFilter")
    public O findCartoesByFilter(@RequestBody I inputFilter) {
        return getService().findAllByFilter(inputFilter);
    }

    @PostMapping
    public T save(@RequestBody T object) {
        this.preSave(object);
        return getService().save(object);
    }

    void preSave(T object) {}

    @GetMapping("{id}")
    public T findone(@PathVariable("id") ID id) {
        return getService().findOne(id);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") ID id) {
        getService().delete(id);
    }

    @PostMapping("deleteObjects")
    public void deleteObjects(@RequestBody DeleteInput id) {
        getService().deleteObjectsById((List<ID>) id.getId());
    }

    @GetMapping("exists/{id}")
    public boolean exists(@PathVariable ID id) {
        return getService().exists(id);
    }

    @GetMapping("count")
    public long count() {
        return getService().count();
    }

    @GetMapping("page")
    public Page<T> findAllPaged(@RequestParam("page") int page,
                                @RequestParam("size") int size,
                                @RequestParam(required = false) String order,
                                @RequestParam(required = false) Boolean asc) {
        PageRequest pageRequest = PageRequest.of(page, size);
        if (order != null && asc != null) {
            pageRequest = PageRequest.of(page, size, asc ? Sort.Direction.ASC : Sort.Direction.DESC, order);
        }
        return getService().findAll(pageRequest);
    }

    @GetMapping("complete")
    public List<T> complete(@RequestParam("query") String query) {
        return getService().complete(query);
    }
}

