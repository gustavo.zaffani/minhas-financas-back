package br.com.gz.minhasfinancas.api.predicate;

import br.com.gz.minhasfinancas.api.model.QUsuario;
import br.com.gz.minhasfinancas.api.util.PredicateBase;

import java.util.Objects;

public class UsuarioPredicate extends PredicateBase<UsuarioPredicate> {

    public UsuarioPredicate filterNome(String nome) {
        if (Objects.nonNull(nome)) {
            getBooleanBuilder().and(QUsuario.usuario1.nome.toLowerCase().like("%" + nome.toLowerCase() + "%"));
        }
        return this;
    }

    public UsuarioPredicate filterUsuario(String usuario) {
        if (Objects.nonNull(usuario)) {
            getBooleanBuilder().and(QUsuario.usuario1.usuario.toLowerCase().like("%" + usuario.toLowerCase() + "%"));
        }
        return this;
    }

    public UsuarioPredicate filterEmail(String email) {
        if (Objects.nonNull(email)) {
            getBooleanBuilder().and(QUsuario.usuario1.email.like("%" + email.toLowerCase() + "%"));
        }
        return this;
    }
}
