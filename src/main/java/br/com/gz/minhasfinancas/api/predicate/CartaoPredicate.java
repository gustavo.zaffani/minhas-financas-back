package br.com.gz.minhasfinancas.api.predicate;

import br.com.gz.minhasfinancas.api.model.QCartao;
import br.com.gz.minhasfinancas.api.util.PredicateBase;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Objects;

public class CartaoPredicate extends PredicateBase<CartaoPredicate> {

    public CartaoPredicate filterCodigo(Long codigo) {
        if (Objects.nonNull(codigo)) {
            getBooleanBuilder().and(QCartao.cartao.codigo.eq(codigo));
        }
        return this;
    }

    public CartaoPredicate filterDescricao(String descricao) {
        if (StringUtils.hasText(descricao)) {
            getBooleanBuilder().and(QCartao.cartao.descricao.toLowerCase().like(getLikeFormatted(descricao)));
        }
        return this;
    }

    public CartaoPredicate filterDtVencimento(Integer dtVencimento) {
        if (Objects.nonNull(dtVencimento)) {
            getBooleanBuilder().and(QCartao.cartao.dtVencimento.eq(dtVencimento));
        }
        return this;
    }

    public CartaoPredicate filterMelhorDataCompra(Integer melhorDataCompra) {
        if (Objects.nonNull(melhorDataCompra)) {
            getBooleanBuilder().and(QCartao.cartao.melhorDataCompra.eq(melhorDataCompra));
        }
        return this;
    }

    public CartaoPredicate filterLimite(BigDecimal limite) {
        if (Objects.nonNull(limite)) {
            getBooleanBuilder().and(QCartao.cartao.limite.eq(limite));
        }
        return this;
    }
}
