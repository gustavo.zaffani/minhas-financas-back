package br.com.gz.minhasfinancas.api.enumeration;

public enum EnumStatusParcela {
    PAGO,
    PENDENTE,
    ATRASADO
}
