package br.com.gz.minhasfinancas.api.predicate;

import br.com.gz.minhasfinancas.api.model.QPessoa;
import br.com.gz.minhasfinancas.api.util.PredicateBase;

import java.util.Objects;

public class PessoaPredicate extends PredicateBase<PessoaPredicate> {

    public PessoaPredicate filterCodigo(Long codigo) {
        if (Objects.nonNull(codigo)) {
            getBooleanBuilder().and(QPessoa.pessoa.codigo.eq(codigo));
        }
        return this;
    }

    public PessoaPredicate filterNome(String nome) {
        if (Objects.nonNull(nome)) {
            getBooleanBuilder().and(QPessoa.pessoa.nome.toLowerCase().like("%" + nome.toLowerCase() + "%"));
        }
        return this;
    }
}
