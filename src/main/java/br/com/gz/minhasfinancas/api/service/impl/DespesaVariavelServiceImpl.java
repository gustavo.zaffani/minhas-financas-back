package br.com.gz.minhasfinancas.api.service.impl;

import br.com.gz.minhasfinancas.api.dto.input.DespesaVariavelFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.DespesaVariavelFilterOutput;
import br.com.gz.minhasfinancas.api.enumeration.EnumStatusDespesa;
import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import br.com.gz.minhasfinancas.api.predicate.DespesaVariavelPredicate;
import br.com.gz.minhasfinancas.api.repository.DespesaVariavelRepository;
import br.com.gz.minhasfinancas.api.service.DespesaVariavelService;
import br.com.gz.minhasfinancas.api.service.ParcelaService;
import br.com.gz.minhasfinancas.api.util.Utils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class DespesaVariavelServiceImpl extends CrudServiceImpl<DespesaVariavel, DespesaVariavelFilterInput, DespesaVariavelFilterOutput, UUID>
    implements DespesaVariavelService {

    @Autowired
    private DespesaVariavelRepository despesaVariavelRepository;

    @Autowired
    private ParcelaService parcelaService;

    @Override
    protected JpaRepository<DespesaVariavel, UUID> getRepository() {
        return despesaVariavelRepository;
    }

    @Override
    public DespesaVariavelFilterOutput findAllByFilter(DespesaVariavelFilterInput inputFilter) {
        PageRequest page = Utils.getPageRequest(inputFilter.getPageRequest());
        OrderSpecifier<?> orderBy = Utils.getOrderBy(inputFilter.getPageRequest());

        List<DespesaVariavel> listOutput = despesaVariavelRepository.findAllByPredicate(getPredicate(inputFilter), page, orderBy);
        Long totalElements = despesaVariavelRepository.getTotalElements(getPredicate(inputFilter));
        return new DespesaVariavelFilterOutput(totalElements, listOutput);
    }

    private Predicate getPredicate(DespesaVariavelFilterInput inputFilter) {
        return new DespesaVariavelPredicate()
                .buildPredicate()
                .filterDescricao(inputFilter.getDescricao())
                .filterVlrDespesa(inputFilter.getVlrDespesa())
                .filterFormaPagamento(inputFilter.getFormaPagamento())
                .filterStatus(inputFilter.getStatusDespesa())
                .getPredicate();
    }

    @Override
    public DespesaVariavel saveDespesaVariavel(DespesaVariavel despesaVariavel) {
        if (Objects.isNull(despesaVariavel.getId())) {
            despesaVariavel.setDtCadastro(LocalDateTime.now());
            despesaVariavel.setStatusDespesa(EnumStatusDespesa.PENDENTE);
        }
        despesaVariavel.setParcelas(parcelaService.gerarParcelasByDespesaVariavel(despesaVariavel));
        return despesaVariavelRepository.save(despesaVariavel);
    }
}
