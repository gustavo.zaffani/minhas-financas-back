package br.com.gz.minhasfinancas.api.controller;

import br.com.gz.minhasfinancas.api.dto.input.UsuarioFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.UsuarioFilterOutput;
import br.com.gz.minhasfinancas.api.model.Usuario;
import br.com.gz.minhasfinancas.api.service.CrudService;
import br.com.gz.minhasfinancas.api.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("usuario")
public class UsuarioController extends CrudController<Usuario, UsuarioFilterInput, UsuarioFilterOutput, UUID> {

    @Autowired
    private UsuarioService usuarioService;

    @Override
    protected CrudService<Usuario, UsuarioFilterInput, UsuarioFilterOutput, UUID> getService() {
        return usuarioService;
    }
}
