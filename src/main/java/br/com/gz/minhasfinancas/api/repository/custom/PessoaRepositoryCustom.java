package br.com.gz.minhasfinancas.api.repository.custom;

import br.com.gz.minhasfinancas.api.model.Pessoa;

public interface PessoaRepositoryCustom extends CustomRepository<Pessoa> {
}
