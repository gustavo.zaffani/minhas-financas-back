package br.com.gz.minhasfinancas.api.enumeration;

public enum EnumTipoCartao {
    CREDITO,
    DEBITO
}
