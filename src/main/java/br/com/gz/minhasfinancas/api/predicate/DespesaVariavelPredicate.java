package br.com.gz.minhasfinancas.api.predicate;

import br.com.gz.minhasfinancas.api.enumeration.EnumFormaPagamento;
import br.com.gz.minhasfinancas.api.enumeration.EnumStatusDespesa;
import br.com.gz.minhasfinancas.api.model.QDespesaVariavel;
import br.com.gz.minhasfinancas.api.util.PredicateBase;

import java.math.BigDecimal;
import java.util.Objects;

public class DespesaVariavelPredicate extends PredicateBase<DespesaVariavelPredicate> {

    public DespesaVariavelPredicate filterDescricao(String descricao) {
        if (Objects.nonNull(descricao)) {
            getBooleanBuilder().and(QDespesaVariavel.despesaVariavel.descricao.toLowerCase().like("%" + descricao.toLowerCase() + "%"));
        }
        return this;
    }

    public DespesaVariavelPredicate filterVlrDespesa(BigDecimal vlrDespesa) {
        if (Objects.nonNull(vlrDespesa)) {
            getBooleanBuilder().and(QDespesaVariavel.despesaVariavel.vlrDespesa.eq(vlrDespesa));
        }
        return this;
    }

    public DespesaVariavelPredicate filterFormaPagamento(EnumFormaPagamento formaPagamento) {
        if (Objects.nonNull(formaPagamento)) {
            getBooleanBuilder().and(QDespesaVariavel.despesaVariavel.formaPagamento.eq(formaPagamento));
        }
        return this;
    }

    public DespesaVariavelPredicate filterStatus(EnumStatusDespesa statusDespesa) {
        if (Objects.nonNull(statusDespesa)) {
            getBooleanBuilder().and(QDespesaVariavel.despesaVariavel.statusDespesa.eq(statusDespesa));
        }
        return this;
    }
}
