package br.com.gz.minhasfinancas.api.dto.input;

import br.com.gz.minhasfinancas.api.util.PageRequest;
import lombok.Data;

@Data
public class InputDefault {

    private PageRequest pageRequest;
}
