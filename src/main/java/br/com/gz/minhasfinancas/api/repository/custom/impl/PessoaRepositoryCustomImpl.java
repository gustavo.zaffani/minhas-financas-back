package br.com.gz.minhasfinancas.api.repository.custom.impl;

import br.com.gz.minhasfinancas.api.model.Pessoa;
import br.com.gz.minhasfinancas.api.model.QPessoa;
import br.com.gz.minhasfinancas.api.repository.custom.PessoaRepositoryCustom;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

public class PessoaRepositoryCustomImpl extends CustomRepositoryImpl<Pessoa>
        implements PessoaRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public List<Pessoa> findAllByPredicate(Predicate predicate, PageRequest pageRequest, OrderSpecifier<?> order) {
        return getBaseQuery(predicate)
                .orderBy(getOrderBy(order))
                .offset(pageRequest.getOffset())
                .limit(pageRequest.getPageSize())
                .fetch();
    }

    @Override
    public JPAQuery<Pessoa> getBaseQuery(Predicate predicate) {
        return new JPAQuery<>(em)
                .select(QPessoa.pessoa)
                .from(QPessoa.pessoa)
                .where(predicate);
    }

    @Override
    public OrderSpecifier<?> getOrderBy(OrderSpecifier<?> orderBy) {
        if (Objects.isNull(orderBy)) {
            return new OrderSpecifier<>(Order.DESC, QPessoa.pessoa.codigo);
        }
        return orderBy;
    }
}
