package br.com.gz.minhasfinancas.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "categoria")
public class Categoria {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull(message = "O campo 'código' não pode ser nulo.")
    @Column(name = "codigo", nullable = false)
    private Long codigo;

    @NotEmpty(message = "O campo 'descrição' não pode ser vazio.")
    @Column(name = "descricao", nullable = false)
    private String descricao;
}
