package br.com.gz.minhasfinancas.api.repository;

import br.com.gz.minhasfinancas.api.model.Usuario;
import br.com.gz.minhasfinancas.api.repository.custom.UsuarioRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, UUID>, UsuarioRepositoryCustom {
}
