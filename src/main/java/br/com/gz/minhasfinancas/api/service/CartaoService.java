package br.com.gz.minhasfinancas.api.service;

import br.com.gz.minhasfinancas.api.dto.input.CartaoFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.CartaoFilterOutput;
import br.com.gz.minhasfinancas.api.model.Cartao;

import java.util.UUID;

public interface CartaoService extends CrudService<Cartao, CartaoFilterInput, CartaoFilterOutput, UUID> {
}
