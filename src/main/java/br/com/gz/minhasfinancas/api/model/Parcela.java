package br.com.gz.minhasfinancas.api.model;

import br.com.gz.minhasfinancas.api.config.LocalDateDeserializer;
import br.com.gz.minhasfinancas.api.config.LocalDateSerializer;
import br.com.gz.minhasfinancas.api.enumeration.EnumStatusParcela;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "parcela")
public class Parcela {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(name = "sequencia", nullable = false)
    private Integer sequencia;

    @Column(name = "qtdparcela", nullable = false)
    private Integer qtdParcela;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Column(name = "dtvencimento", nullable = false)
    private LocalDate dtVencimento;

    @Column(name = "vlrparcela", nullable = false)
    private BigDecimal vlrParcela;

    @Enumerated(EnumType.STRING)
    @Column(name = "statusparcela", nullable = false)
    private EnumStatusParcela statusParcela;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "despesavariavel_id", referencedColumnName = "id")
    private DespesaVariavel despesaVariavel;
}
