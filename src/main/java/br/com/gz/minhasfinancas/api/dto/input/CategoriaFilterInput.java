package br.com.gz.minhasfinancas.api.dto.input;

import lombok.Data;

@Data
public class CategoriaFilterInput extends InputDefault {

    private Long codigo;
    private String descricao;
}
