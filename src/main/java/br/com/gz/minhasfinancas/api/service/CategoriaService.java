package br.com.gz.minhasfinancas.api.service;

import br.com.gz.minhasfinancas.api.dto.input.CategoriaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.CategoriaFilterOutput;
import br.com.gz.minhasfinancas.api.model.Categoria;

import java.util.UUID;

public interface CategoriaService extends CrudService<Categoria, CategoriaFilterInput, CategoriaFilterOutput, UUID> {
}
