package br.com.gz.minhasfinancas.api.repository.custom;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface CustomRepository<T> {

    List<T> findAllByPredicate(Predicate predicate, PageRequest pageRequest, OrderSpecifier<?> order);

    JPAQuery<T> getBaseQuery(Predicate predicate);

    OrderSpecifier<?> getOrderBy(OrderSpecifier<?> orderBy);

    Long getTotalElements(Predicate predicate);
}
