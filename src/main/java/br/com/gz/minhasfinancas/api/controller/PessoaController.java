package br.com.gz.minhasfinancas.api.controller;

import br.com.gz.minhasfinancas.api.dto.input.PessoaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.PessoaFilterOutput;
import br.com.gz.minhasfinancas.api.model.Pessoa;
import br.com.gz.minhasfinancas.api.service.CrudService;
import br.com.gz.minhasfinancas.api.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("pessoa")
public class PessoaController extends CrudController<Pessoa, PessoaFilterInput, PessoaFilterOutput, UUID> {

    @Autowired
    private PessoaService pessoaService;

    @Override
    protected CrudService<Pessoa, PessoaFilterInput, PessoaFilterOutput, UUID> getService() {
        return pessoaService;
    }
}
