package br.com.gz.minhasfinancas.api.model;

import br.com.gz.minhasfinancas.api.config.LocalDateDeserializer;
import br.com.gz.minhasfinancas.api.config.LocalDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "usuario")
public class Usuario {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotEmpty(message = "O campo 'nome' não pode ser vazio.")
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotEmpty(message = "O campo 'usuario' não pode ser vazio.")
    @Column(name = "usuario", nullable = false, unique = true)
    private String usuario;

    @NotEmpty(message = "O campo 'senha' não pode ser vazio.")
    @Column(name = "senha", nullable = false)
    private String senha;

    @Column(name = "email")
    private String email;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Column(name = "dtnascimento")
    private LocalDate dtNascimento;
}
