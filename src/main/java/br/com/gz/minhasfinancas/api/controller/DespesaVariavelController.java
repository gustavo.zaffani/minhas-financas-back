package br.com.gz.minhasfinancas.api.controller;

import br.com.gz.minhasfinancas.api.dto.input.DespesaVariavelFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.DespesaVariavelFilterOutput;
import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import br.com.gz.minhasfinancas.api.service.CrudService;
import br.com.gz.minhasfinancas.api.service.DespesaVariavelService;
import br.com.gz.minhasfinancas.api.validator.DespesaVariavelValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("despesa-variavel")
public class DespesaVariavelController extends CrudController<DespesaVariavel, DespesaVariavelFilterInput, DespesaVariavelFilterOutput, UUID> {

    @Autowired
    private DespesaVariavelService despesaVariavelService;

    @Autowired
    private DespesaVariavelValidator despesaVariavelValidator;

    @Override
    protected CrudService<DespesaVariavel, DespesaVariavelFilterInput, DespesaVariavelFilterOutput, UUID> getService() {
        return despesaVariavelService;
    }

    @PostMapping("save-custom")
    public DespesaVariavel save(@RequestBody DespesaVariavel despesaVariavel) {
        despesaVariavelValidator.validate(despesaVariavel);
        return despesaVariavelService.saveDespesaVariavel(despesaVariavel);
    }
}
