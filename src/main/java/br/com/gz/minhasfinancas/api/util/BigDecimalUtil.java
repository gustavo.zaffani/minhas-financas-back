package br.com.gz.minhasfinancas.api.util;

import java.math.BigDecimal;
import java.util.List;

public class BigDecimalUtil {

    public static BigDecimal sumValues(List<BigDecimal> values) {
        BigDecimal total = BigDecimal.ZERO;
        for (BigDecimal value : values) {
            total = total.add(value);
        }
        return total;
    }
}
