package br.com.gz.minhasfinancas.api.dto.input;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class DeleteInput {

    private List<UUID> id;
}
