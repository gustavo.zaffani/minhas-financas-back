package br.com.gz.minhasfinancas.api.validator;

import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class DespesaVariavelValidator {

    public void validate(DespesaVariavel despesaVariavel) {
        validRateio(despesaVariavel);
    }

    private void validRateio(DespesaVariavel despesaVariavel) {
        if (CollectionUtils.isEmpty(despesaVariavel.getRateios())) {
            throw new RuntimeException("Obrigado informar o rateio");
        }
    }
}
