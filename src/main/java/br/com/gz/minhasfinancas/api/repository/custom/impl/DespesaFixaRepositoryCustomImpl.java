package br.com.gz.minhasfinancas.api.repository.custom.impl;

import br.com.gz.minhasfinancas.api.model.DespesaFixa;
import br.com.gz.minhasfinancas.api.model.QDespesaFixa;
import br.com.gz.minhasfinancas.api.repository.custom.DespesaFixaRepositoryCustom;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.PageRequest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

public class DespesaFixaRepositoryCustomImpl extends CustomRepositoryImpl<DespesaFixa>
        implements DespesaFixaRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DespesaFixa> findAllByPredicate(Predicate predicate, PageRequest pageRequest, OrderSpecifier<?> order) {
        return getBaseQuery(predicate)
                .orderBy(getOrderBy(order))
                .offset(pageRequest.getOffset())
                .limit(pageRequest.getPageSize())
                .fetch();
    }

    @Override
    public JPAQuery<DespesaFixa> getBaseQuery(Predicate predicate) {
        return new JPAQuery<QDespesaFixa>(em)
                .select(QDespesaFixa.despesaFixa)
                .from(QDespesaFixa.despesaFixa)
                .where(predicate);
    }

    @Override
    public OrderSpecifier<?> getOrderBy(OrderSpecifier<?> orderBy) {
        if (Objects.isNull(orderBy)) {
            return new OrderSpecifier<>(Order.ASC, QDespesaFixa.despesaFixa.diaVencimento);
        }
        return orderBy;
    }
}
