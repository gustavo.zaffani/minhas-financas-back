package br.com.gz.minhasfinancas.api.repository.custom.impl;

import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import br.com.gz.minhasfinancas.api.model.QDespesaVariavel;
import br.com.gz.minhasfinancas.api.repository.custom.DespesaVariavelRepositoryCustom;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.PageRequest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

public class DespesaVariavelRepositoryCustomImpl extends CustomRepositoryImpl<DespesaVariavel>
        implements DespesaVariavelRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DespesaVariavel> findAllByPredicate(Predicate predicate, PageRequest pageRequest, OrderSpecifier<?> order) {
        return getBaseQuery(predicate)
                .orderBy(getOrderBy(order))
                .offset(pageRequest.getOffset())
                .limit(pageRequest.getPageSize())
                .fetch();
    }

    @Override
    public JPAQuery<DespesaVariavel> getBaseQuery(Predicate predicate) {
        return new JPAQuery<QDespesaVariavel>(em)
                .select(QDespesaVariavel.despesaVariavel)
                .from(QDespesaVariavel.despesaVariavel)
                .where(predicate);
    }

    @Override
    public OrderSpecifier<?> getOrderBy(OrderSpecifier<?> orderBy) {
        if (Objects.isNull(orderBy)) {
            return new OrderSpecifier<>(Order.ASC, QDespesaVariavel.despesaVariavel.diaVencimento);
        }
        return orderBy;
    }
}
