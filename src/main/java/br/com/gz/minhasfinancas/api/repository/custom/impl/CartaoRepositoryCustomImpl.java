package br.com.gz.minhasfinancas.api.repository.custom.impl;

import br.com.gz.minhasfinancas.api.model.Cartao;
import br.com.gz.minhasfinancas.api.model.QCartao;
import br.com.gz.minhasfinancas.api.repository.custom.CartaoRepositoryCustom;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

public class CartaoRepositoryCustomImpl extends CustomRepositoryImpl<Cartao>
        implements CartaoRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public List<Cartao> findAllByPredicate(Predicate predicate, PageRequest pageRequest, OrderSpecifier<?> order) {
        return getBaseQuery(predicate)
                .orderBy(getOrderBy(order))
                .offset(pageRequest.getOffset())
                .limit(pageRequest.getPageSize())
                .fetch();
    }

    @Override
    public JPAQuery<Cartao> getBaseQuery(Predicate predicate) {
        return new JPAQuery<>(em)
                .select(QCartao.cartao)
                .from(QCartao.cartao)
                .where(predicate);
    }

    @Override
    public OrderSpecifier<?> getOrderBy(OrderSpecifier<?> orderBy) {
        if (Objects.isNull(orderBy)) {
            return new OrderSpecifier<>(Order.DESC, QCartao.cartao.codigo);
        }
        return orderBy;
    }
}
