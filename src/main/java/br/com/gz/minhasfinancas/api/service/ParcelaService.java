package br.com.gz.minhasfinancas.api.service;

import br.com.gz.minhasfinancas.api.model.DespesaVariavel;
import br.com.gz.minhasfinancas.api.model.Parcela;

import java.util.List;

public interface ParcelaService {

    List<Parcela> gerarParcelasByDespesaVariavel(DespesaVariavel despesaVariavel);
}
