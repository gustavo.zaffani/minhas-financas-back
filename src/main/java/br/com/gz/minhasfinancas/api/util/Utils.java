package br.com.gz.minhasfinancas.api.util;

import br.com.gz.minhasfinancas.api.enumeration.EnumTypeOrder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.Expressions;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

@UtilityClass
public class Utils {

    public static PageRequest getPageRequest(br.com.gz.minhasfinancas.api.util.PageRequest pageRequest) {
        if (Objects.isNull(pageRequest) ||
                Objects.isNull(pageRequest.getSize()) ||
                Objects.isNull(pageRequest.getOffset())) {
            return PageRequest.of(0, 10);
        }
        return PageRequest.of(pageRequest.getOffset().intValue(), pageRequest.getSize().intValue());
    }

    public static OrderSpecifier<?> getOrderBy(br.com.gz.minhasfinancas.api.util.PageRequest pageRequest) {
        if (Objects.isNull(pageRequest) ||
                Objects.isNull(pageRequest.getFieldOrder()) ||
                Objects.isNull(pageRequest.getTypeOrder()))
            return null;
        return createOrder(pageRequest.getFieldOrder(), convertOrder(pageRequest.getTypeOrder()));
    }

    @SuppressWarnings("rawtypes")
    private static OrderSpecifier<String> createOrder(String fieldName, Order order) {
        return new OrderSpecifier<>(order, Expressions.stringPath(fieldName));
    }

    public static Order convertOrder(EnumTypeOrder order) {
        switch (order) {
            case ASC:
                return Order.ASC;
            case DESC:
                return Order.DESC;
            default:
                return null;
        }
    }

    public static Integer getTotalPage(Integer size, Long totalElements) {
        if (Objects.nonNull(size) && Objects.nonNull(totalElements)) {
            BigDecimal result = BigDecimal.valueOf(totalElements)
                    .divide(BigDecimal.valueOf(size), MathContext.DECIMAL32)
                    .setScale(0, RoundingMode.UP);
            return Integer.valueOf(result.toString());
        }

        return 0;
    }

    public static LocalDate getNextVencimentoCartao(Integer melhorData, Integer dtVencimento, LocalDate dtDespesa) {
        int diaDespesa = dtDespesa.getDayOfMonth();
        LocalDate localDateBase = LocalDate.of(dtDespesa.getYear(), dtDespesa.getMonth(), dtVencimento);
        if (diaDespesa >= melhorData) {
            localDateBase = localDateBase.plusMonths(1L);
        }
        return localDateBase;
    }

    public static LocalDate getNextVencimentoCartaoByParcela(Integer melhorData,
                                                             Integer dtVencimento,
                                                             LocalDate dtDespesa,
                                                             Integer parcelaVigente) {
        LocalDate vencimentoBase = getNextVencimentoCartao(melhorData, dtVencimento, dtDespesa);
        if (parcelaVigente != 1) {
            return vencimentoBase.plusMonths(parcelaVigente - 1);
        }
        return vencimentoBase;
    }

    public static LocalDate getNextVencimentoBoletoByParcela(Integer diaVencimento, Integer parcelaVigente, LocalDate dtDespesa) {
        LocalDate localDateBase = LocalDate.of(dtDespesa.getYear(), dtDespesa.getMonth(), diaVencimento);
        return localDateBase.plusMonths(parcelaVigente);
    }

}
