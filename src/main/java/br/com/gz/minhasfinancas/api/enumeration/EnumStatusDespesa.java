package br.com.gz.minhasfinancas.api.enumeration;

public enum EnumStatusDespesa {
    PENDENTE,
    PARCIALMENTE_PAGO,
    TOTALMENTE_PAGO
}
