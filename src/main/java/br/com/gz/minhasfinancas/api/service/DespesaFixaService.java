package br.com.gz.minhasfinancas.api.service;

import br.com.gz.minhasfinancas.api.dto.input.DespesaFixaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.DespesaFixaFilterOutput;
import br.com.gz.minhasfinancas.api.model.DespesaFixa;

import java.util.UUID;

public interface DespesaFixaService extends CrudService<DespesaFixa, DespesaFixaFilterInput, DespesaFixaFilterOutput, UUID> {
}
