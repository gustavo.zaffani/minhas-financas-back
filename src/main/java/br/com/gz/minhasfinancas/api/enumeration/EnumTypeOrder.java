package br.com.gz.minhasfinancas.api.enumeration;

public enum EnumTypeOrder {
    ASC,
    DESC;
}
