package br.com.gz.minhasfinancas.api.service;

import br.com.gz.minhasfinancas.api.dto.input.PessoaFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.PessoaFilterOutput;
import br.com.gz.minhasfinancas.api.model.Pessoa;

import java.util.UUID;

public interface PessoaService extends CrudService<Pessoa, PessoaFilterInput, PessoaFilterOutput, UUID> {
}
