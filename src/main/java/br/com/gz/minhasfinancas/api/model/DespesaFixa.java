package br.com.gz.minhasfinancas.api.model;

import br.com.gz.minhasfinancas.api.config.LocalDateDeserializer;
import br.com.gz.minhasfinancas.api.config.LocalDateSerializer;
import br.com.gz.minhasfinancas.api.config.LocalDateTimeDeserializer;
import br.com.gz.minhasfinancas.api.config.LocalDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Table(name = "despesaFixa")
public class DespesaFixa {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotEmpty(message = "O campo 'descricao' não pode ser vazio.")
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @NotNull
    @Column(name = "dtcadastro", nullable = false)
    private LocalDateTime dtCadastro;

    @NotNull(message = "O campo 'valor da despesa' não pode ser nulo.")
    @Column(name = "vlrdespesa", nullable = false)
    private BigDecimal vlrDespesa;

    @NotNull(message = "O campo 'dia do vencimento' não pode ser nulo.")
    @Column(name = "diavencimento", nullable = false)
    private Integer diaVencimento;

    @Column(name = "observacao", length = 500)
    private String observacao;
}
