package br.com.gz.minhasfinancas.api.repository.custom;

import br.com.gz.minhasfinancas.api.model.Categoria;

public interface CategoriaRepositoryCustom extends CustomRepository<Categoria> {
}
