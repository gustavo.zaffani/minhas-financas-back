package br.com.gz.minhasfinancas.api.service.impl;

import br.com.gz.minhasfinancas.api.dto.input.UsuarioFilterInput;
import br.com.gz.minhasfinancas.api.dto.output.UsuarioFilterOutput;
import br.com.gz.minhasfinancas.api.model.Usuario;
import br.com.gz.minhasfinancas.api.predicate.UsuarioPredicate;
import br.com.gz.minhasfinancas.api.repository.UsuarioRepository;
import br.com.gz.minhasfinancas.api.service.UsuarioService;
import br.com.gz.minhasfinancas.api.util.Utils;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UsuarioServiceImpl extends CrudServiceImpl<Usuario, UsuarioFilterInput, UsuarioFilterOutput, UUID>
        implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    protected JpaRepository<Usuario, UUID> getRepository() {
        return usuarioRepository;
    }

    @Override
    public UsuarioFilterOutput findAllByFilter(UsuarioFilterInput inputFilter) {
        PageRequest page = Utils.getPageRequest(inputFilter.getPageRequest());
        OrderSpecifier<?> orderBy = Utils.getOrderBy(inputFilter.getPageRequest());

        List<Usuario> listOutput = usuarioRepository.findAllByPredicate(getPredicate(inputFilter), page, orderBy);
        Long totalElements = usuarioRepository.getTotalElements(getPredicate(inputFilter));

        return new UsuarioFilterOutput(listOutput, totalElements);
    }

    private Predicate getPredicate(UsuarioFilterInput usuarioFilterInput) {
        return new UsuarioPredicate()
                .buildPredicate()
                .filterEmail(usuarioFilterInput.getEmail())
                .filterNome(usuarioFilterInput.getNome())
                .filterUsuario(usuarioFilterInput.getUsuario())
                .getPredicate();
    }
}
